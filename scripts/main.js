var fs = require('fs');

window.$ = window.jQuery = require('jquery');

// Get arrays from file
var arrayFile = require(__dirname + '/scripts/words.js');

var soundsArray = arrayFile.soundsArray;
var wordsArray = arrayFile.wordArray;

// Global Arrays/Variables
const SIZE = 108;
var currentPosition = SIZE / 2;
currentPosition = Math.round(currentPosition);
var lowestPosition = 1;
var highestPosition = SIZE;
var mainArray = new Array(SIZE);
var outputArray = new Array();
var inputName;
var moveCount = 0;
var wasCorrect;
var isFinished = false;

var listeningForInput = false;

window.onload = function(){

    initializeArray();

    $("#main").hide();


    //Event listeners

    // Sort event listeners that trigger when the tester presses the up or down key
    document.addEventListener('keyup', function(event){
        if(listeningForInput){
            if(event.keyCode == 40 || event.keyCode == 38){
                sort(event.keyCode);
            }
        }
    });

    // Resets the experiment when the tester presses this button 
    $("#reset").click(function(){ 
        // Set the array up to initial values
        initializeArray(); 

        // Reveal the welocme page
        $("#welcome").show();

        // Hide the output div
        $("#main").hide();

        //Stop listening for button presses
        listeningForInput = false;
    });

    $("#submit").click(function(){

        // Get the participant code
        inputName = $("#user_input").val();
        $("#subject").html(inputName);
        console.log(inputName);

        // Hide the landing page
        $("#welcome").hide();

        // Show the output
        $("#main").show();
       
        //Start listening for input 
        listeningForInput = true;
    });
}

function sort(buttonID){

    if(buttonID == 38){
        wasCorrect = true;
        recordOutput();
        sortLeft();        
    } else if(buttonID == 40){
        wasCorrect = false;
        recordOutput();
        sortRight();
    }

    displayOutput();
    updateImage();
    moveCount++;
    checkIfFinished();
}

function sortLeft(){

    highestPosition = currentPosition;
    currentPosition = Math.floor((highestPosition + lowestPosition) / 2);
    
}

function sortRight(){

    lowestPosition = currentPosition;
    currentPosition = Math.round((highestPosition + lowestPosition) / 2);

}

function initializeArray(){

    currentPosition = SIZE / 2;
    lowestPosition = 1;
    highestPosition = SIZE;

    for(var i = 0; i < SIZE; i++){
        mainArray[i] = i + 1;
    }

    displayOutput();
    updateImage();
    outputArray = [];
    moveCount = 1; 
    isFinished = false;
    //inputName = prompt("Enter the student's name:")
}

function displayOutput(){
    $("#main").html("Lowest: "+ lowestPosition + " Current: " + currentPosition + " Highest: " + highestPosition + " " + randomString());
}

function checkIfFinished(){
    if (isFinished){
        var displayName = writeFile();
        alert("Done, results saved as " + displayName);
        isFinished = false;
    } else {
        if((currentPosition == highestPosition) || (currentPosition == lowestPosition)){
            isFinished = true;
        }
    }
   
}

function recordOutput(){
    // Create a new object to add to the output array

    // Create the sound string
    var objectToAdd = {
        "move": moveCount,
        "correct": wasCorrect,
        "current": currentPosition,
        "sound": soundsArray[Math.round(currentPosition)] + " in " + wordsArray[Math.round(currentPosition)]
    }

    var stringToAdd = moveCount + "," + wasCorrect + "," + currentPosition + "," + objectToAdd['sound'] + "\r\n";

    //Add object to the output array
    outputArray.push(stringToAdd);

    console.log(outputArray);
}

function updateImage(){
   $('#image').attr('src','./images/'+ Math.floor(currentPosition) + '.jpg' );
}

function randomString(){
    var stringToReturn = wordsArray[Math.round(currentPosition)];

    stringToReturn += Math.round(Math.random() * 10);
    // Generate a Random string
    // stringToReturn += Math.random().toString(36).substring(7);    

    // Add a random number at the end
    stringToReturn += Math.round(Math.random() * 10);

    // Add the sound to the end of the word
    stringToReturn += soundsArray[Math.round(currentPosition)];

    return stringToReturn;
}

function writeFile(){

    // create the file name

    // Put together the subject's name and the current date and time

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + '_' + today.getMinutes() + '_' + today.getSeconds();
    var datetime = date + "_" + time;

    var filename = inputName + "_" + datetime;
    var toWrite;

    // Check to see if the ouput directory is made
    // If it isn't, make one
    var dir = './output';

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    // Make the string that will be written to the file
    outputArray.forEach(function(element)
    {
        //toWrite += JSON.stringify(element, null, 4);
        toWrite += element;
    });

    console.log(toWrite);

    try {fs.writeFileSync(dir + '/' + filename + '.txt', toWrite, 'utf-8');}
    catch(e) { console.log(e); }

    return filename;
}
